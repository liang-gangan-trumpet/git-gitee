//
//  AppDelegate.h
//  weathercast
//
//  Created by 刘鹏 on 2019/6/26.
//  Copyright © 2019 刘鹏. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

